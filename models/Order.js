const mongoose = require('mongoose')
const orderSchema = new mongoose.Schema({

	totalAmount: {
		type: Number,
		required : [true, 'Total Amount is is required']
	},

	purchasedOn: {
		type: Date,
		default : new Date()
	},

	user: [
	{
		userId: {
			type: String,
			required : [true, 'User Id is required']
		},

		userEmail : {
			type: String,
			required : [true, 'Email is required']
		}
	}],

	product: [
	{
		productId : {
			type: String,
			required : [true, 'User Id is required']
		},

		productName : {
			type: String,
			required : [true, 'Product name is required']
		}
	}]
	
	
})

module.exports = mongoose.model('Order', orderSchema)