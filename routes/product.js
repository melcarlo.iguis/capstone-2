const express = require('express');
const router = express.Router();
const productController = require('../controllers/product');
const auth = require('../auth');


// - Retrieve all active products - routes
router.get('/activeProduct' , (req, res) =>{
	productController.getAllActice().then(resultFromController => res.send(resultFromController))
})

// - Retrieve single product - routes
router.get('/:productId/get', (req, res)=>{
	productController.getSingleProduct(req.params).then(resultFromController =>res.send(resultFromController))
})


// -  Create Product (Admin only) - routes
router.post('/create', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin) {
		productController.createProduct(req.body).then(resultFromController=> res.send(resultFromController))
	}else{
		return res.send("you're not an admin!, failed to add product")
	}
})

// -  Update Product information (Admin only) - routes
router.put('/:productId/update' , auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){
		productController.updateProduct(req.params, req.body).then(resultFromController=> res.send(resultFromController))
	}else{
		return res.send("you're not an admin!, failed to update a product")
	}
	
})

// - Archive Product (Admin only) - routes
router.put('/:productId/archive' , (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){
		productController.archiveProduct(req.params).then(resultFromController=> res.send(resultFromController))
	}else{
		return res.send("you're not an admin!, failed to archive product")
	}
})

module.exports = router;