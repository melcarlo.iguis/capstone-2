const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// -  Create Product (Admin only) - controller
module.exports.createProduct = (reqBody) => {

	let newProduct = new Product({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	// Conditio for checking product duplicate
	return Product.find({name: reqBody.name}).then(result =>{

		if(result.length > 0 ){
			console.log("Product already exist");
			return("This product name already exist, failed to add new product")
		}else{
			return newProduct.save().then((addProduct,error)=> {
				if(error){
					return false
				}else{
					console.log("Product has been added")
					return true

				}
			})
		}
		
	})
}

// - Retrieve all active products controller
module.exports.getAllActice = () => {
	return Product.find({isActive: true}).then(result =>{
		return result
	})
}

// - Retrieve single product controller
module.exports.getSingleProduct = (reqParams) =>{
	return Product.findById(reqParams.productId).then(result =>{
		return result
	})

}

// -  Update Product information (Admin only) - controller
module.exports.updateProduct = (reqParams, reqBody) =>{

	let updatedProduct = {

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((update,error) =>{
		if(error){
			return false
		}else{
			console.log('product updated successfully')
			return true

		}
	})
}

// // - Archive Product (Admin only) - controller
module.exports.archiveProduct = (reqParams) =>{

	let updatedProduct = {
		isActive : false
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((update,error) =>{
		if(error){
			return false
		}else{
			console.log('product archived successfully,')
			return true
		}
	})
}