const express = require('express');
const router = express.Router();
const userController = require('../controllers/user');
const auth = require('../auth');

// register route
router.post('/register' , (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})


// login route
router.post('/login' , (req,res) =>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

// set user to admin 
router.put('/:userId/setAsAdmin', auth.verify, (req, res)=> {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == true) {
		userController.setAsAdmin(req.params).then(resultFromController=> res.send(resultFromController))
	}else{
		return res.send("you're not an admin!, failed to set to admin")
		return false
	}
})

// - Non-admin User checkout (Create Order)
router.post('/checkout', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	let data = {

		quantity : req.body.quantity,

		userId: userData.id,
		productId: req.body.productId
	}
	
	if(!userData.isAdmin){
		userController.checkout(data).then(resultFromController =>
		res.send(resultFromController));
	}else{
		res.send("you're an admin, you're not allowed to checkout")
	}
})

// - Retrieve authenticated user’s orders
router.get('/getOrder',auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization)

	let data = {

		userId: userData.id
	}

	if(!userData.isAdmin){
		userController.getOrder(data).then(resultFromController =>res.send(resultFromController))
	}else{
		res.send("you're an admin, you're not allowed in this feature")
	}
})

// - Retrieve all orders (Admin only)
router.get('/getAllOrder',auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){
		userController.getAllOrder().then(resultFromController =>res.send(resultFromController))
	}else{
		res.send("you're not an admin, you're not allowed in this feature")
	}
})

module.exports = router;